lazy val commonSettings = Seq(
  organization := "com.example",
  version := "0.1.0",
  scalaVersion := "2.11.4"
)


val derby = "org.apache.derby" % "derby" % "10.4.1.3"
val scalatest = "org.scalatest" %% "scalatest" % "2.2.0"
val junit = "junit" % "junit" % "4.12" % "test"
val testng = "org.testng" % "testng" % "6.8.21" % "test"


val akkaVersion       = "2.3.9"
val sprayVersion      = "1.3.2"

val akkaActor = "com.typesafe.akka" %% "akka-actor"      % akkaVersion
val sprayCan = "io.spray"          %% "spray-can"       % sprayVersion
val sprayRouting = "io.spray"          %% "spray-routing"   % sprayVersion
val sprayJson = "io.spray"          %% "spray-json"      % "1.3.1"
val akkaSlf4j = "com.typesafe.akka" %% "akka-slf4j"      % akkaVersion
val akkaRemote = "com.typesafe.akka" %% "akka-remote" % akkaVersion

lazy val root = (project in file(".")).
  settings(commonSettings: _*).
  settings(
    name := "hello",
    version := "1.0",
    scalaVersion := "2.11.4",
        libraryDependencies += derby,
        libraryDependencies += scalatest,
        libraryDependencies += junit,
        libraryDependencies += testng,
          libraryDependencies += akkaActor,
          libraryDependencies += sprayCan,
          libraryDependencies += sprayRouting,
    libraryDependencies += sprayJson,
    libraryDependencies += akkaSlf4j,
    libraryDependencies += akkaRemote




  )
