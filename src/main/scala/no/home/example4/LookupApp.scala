package no.home.example4

import akka.actor.{Props, ActorSystem}
import com.typesafe.config.ConfigFactory

import scala.util.Random
import scala.concurrent.duration._
/**
 * Created by wojciechprzechrzta on 13/04/15.
 */
object LookupApp {

  def main(args: Array[String]): Unit = {

    val system =
      ActorSystem("LookupSystem", ConfigFactory.load("remotelookup"))
    val remotePath =
      "akka.tcp://CalculatorSystem@127.0.0.1:2552/user/calculator"
    val actor = system.actorOf(Props(classOf[LookupActor], remotePath), "lookupActor")

    println("Started LookupSystem")
    import system.dispatcher
    system.scheduler.schedule(1.second, 1.second) {
      if (Random.nextInt(100) % 2 == 0)
        actor ! Add(Random.nextInt(100), Random.nextInt(100))
      else
        actor ! Subtract(Random.nextInt(100), Random.nextInt(100))
    }
  }

}
