package no.home.example4

import akka.actor.{ActorSystem, Props}
import com.typesafe.config.ConfigFactory


object LookupAppCalc {
  def main(args: Array[String]): Unit = {

    val system = ActorSystem("CalculatorSystem",
      ConfigFactory.load("calculator"))

    system.actorOf(Props[CalculatorActor], "calculator")

    println("Started CalculatorSystem - waiting for messages")
  }


}
