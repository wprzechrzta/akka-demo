package no.home.example1

import java.io.File

import akka.actor.{Actor, ActorSystem, Props}

import scala.io._


object Main {
  def main(args: Array[String]) {
    val system = ActorSystem("word-count-system")

    val master = system.actorOf(Props[WordCountMaster], name="master")
    master ! StartCounting("src/main/resources/", 2)
  }
}

class WordCountMaster extends Actor {

  var fileNames: Seq[String] = Nil
  var sortedCount : Seq[(String, Int)] = Nil

  def receive = {

    case StartCounting(docRoot, numActors) =>

      val workers = for (i <- 0 until numActors)
        yield context.actorOf(Props[WordCountWorker], name = s"worker-${i}")

      fileNames = new File(docRoot).list.map(docRoot + _)

      fileNames.zipWithIndex.foreach( e => {
        workers(e._2 % workers.size) ! FileToCount(e._1)
      })

    case WordCount(fileName, count) =>

      sortedCount = sortedCount :+ (fileName, count)

      sortedCount = sortedCount.sortWith(_._2 < _._2)

      if(sortedCount.size == fileNames.size) {
        println("final result " + sortedCount)

        context.system.shutdown()
      }
  }

  override def postStop(): Unit = {
    println(s"Master actor is stopped: ${self}")
  }


}

case class FileToCount(fileName:String)
case class WordCount(fileName:String, count: Int)
case class StartCounting(docRoot: String, numActors: Int)


class WordCountWorker extends Actor {

  def receive = {
    case FileToCount(fileName:String) =>

      val dataFile = new File(fileName)
      val count = Source.fromFile(dataFile).getLines.foldRight(0)(_.split(" ").size + _)

      sender ! WordCount(fileName, count)
  }

  override def postStop(): Unit = {
    println(s"Worker actor is stopped: ${self}")
  }

}


