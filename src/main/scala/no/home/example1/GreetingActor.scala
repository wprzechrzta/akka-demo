package no.home.example1

import akka.actor.{Props, ActorSystem, Actor}
import akka.actor.Actor.Receive

/**
 * Created by wojciechprzechrzta on 11/04/15.
 */

object Greetings extends App{

case class Name(name: String)

class GreetingActor extends Actor{
  override def receive: Receive = {
    case Name(n) => println("Hello " + n)
  }
}

val system =  ActorSystem("greetings")

  val actor =  system.actorOf(Props[GreetingActor], name = "greeter")

  actor ! Name(" tralala jubudu ")

  Thread.sleep(50)

  system.shutdown()
}
