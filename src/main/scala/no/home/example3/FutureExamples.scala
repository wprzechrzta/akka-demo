package no.home.example3



import scala.concurrent._
import ExecutionContext.Implicits.global

case class EventRequest(tktNr:Int)
case class Event(info: String, location:String)
case class TrafficRequest(location:String)
case class Route(routeInfo:String)


object FutureExamples extends App{

  def eventService(request: EventRequest) = {
    Event("event-1", "Krakow")
  }

  val request = EventRequest(123)

  val futureEvent: Future[Event] = Future {

    eventService(request)
  }

  def callTrafficService(value: Any): Route = {
    println("Fetching traffic info")
    Route("route-via KRK")
  }

  val trafficInfo = futureEvent.map { event =>
     val trafficReq = TrafficRequest(event.location)

     callTrafficService(trafficReq)
  }


  Thread.sleep(1000)

  trafficInfo.foreach(route => println(route.routeInfo))
}
