package no.home.example2

/**
 * Created by jotus on 13/04/15.
 */

import akka.actor.SupervisorStrategy.Restart
import akka.actor._

class LifeCycleHooks extends Actor with ActorLogging {
  log.info("Constructor")

  override def preStart() {
    log.info("preStart")

  }


  override def postStop() {
    log.info("postStop")
  }

  override def preRestart(reason: Throwable,
                          message: Option[Any]) {
    log.info("preRestart")
    super.preRestart(reason, message)
  }

  override def postRestart(reason: Throwable) {
    log.info("postRestart")
    super.postRestart(reason)

  }


  def receive = {
    case "restart" =>
      log.info("lifeActor - Received restartt")
      throw new IllegalStateException("force restart")

    case msg: AnyRef =>
      log.info(s"lifeActor - Received message: $msg")
  }
}

class MasterActor extends Actor with ActorLogging {

  val lifeCycleActor = createChild

  override def supervisorStrategy = OneForOneStrategy() {
    case _ =>
      log.info("Applying restart strategy")
      Restart
  }

  def receive = {
    case msg: AnyRef =>
      log.info(s"forwarding $msg")
      lifeCycleActor.forward(msg)
  }

  def createChild = {
    context.actorOf(Props[LifeCycleHooks], "LifeCycleActor")
  }
}

object LifeCycleHooks extends App {
  val system = ActorSystem("lifecycle")

  val masterActor = system.actorOf(Props[MasterActor], "MasterActor")

  masterActor ! "restart"

  Thread.sleep(2000)
  masterActor ! "hello message"

  system.stop(masterActor)



}
